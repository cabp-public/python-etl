from dotenv import load_dotenv
load_dotenv()

# Same with increased verbosity:
# load_dotenv(verbose=True)

# Explicitly providing path to '.env'
# from pathlib import Path  # python3 only
# env_path = Path('.') / '.env'
# load_dotenv(dotenv_path=env_path)
