import json
import luigi

from app.storages.localstorage import FileReader


class BackUpJSON(luigi.ExternalTask):
    inputPath = luigi.Parameter()
    outputPath = luigi.Parameter()
    fileReader = FileReader()

    def run(self):
        try:
            content_data = self.fileReader.readJSON(self.inputPath)
            content = json.dumps(content_data)

            with self.output().open('w') as file:
                file.write(content)

        except Exception as e:
            # TODO: use an error handler
            # TODO: log exception
            print(e)

        finally:
            # TODO: update document data
            print('BackUpJSON ready...')

    def output(self):
        return luigi.LocalTarget(path=str(self.outputPath))
