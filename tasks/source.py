import json
import luigi

from tasks.backup import BackUpJSON

class SourceJSON(luigi.ExternalTask):
    inputPath = luigi.Parameter()
    backupPath = luigi.Parameter()
    sourcePath = luigi.Parameter()

    def requires(self):
        return BackUpJSON(
            inputPath = self.inputPath,
            backupPath = self.backupPath
        )

    def run(self):
        try:
            with self.input().open('r') as jsonFile:
                jsonContent = json.load(jsonFile)

            content = json.dumps(jsonContent)

            with self.output().open('w') as file:
                file.write(content)

        except Exception as e:
            # TODO: use an error handler
            print(e)

        finally:
            # TODO: update document status
            print('SourceJSON ready...')

    def output(self):
        return luigi.LocalTarget(path = self.sourcePath)
