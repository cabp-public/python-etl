import json
import luigi

from time import sleep
from tasks.backup import BackUpJSON
from app.storages.localstorage import FileReader
from app.mappers.aptos.items import MapItem


class TransformItem(luigi.ExternalTask):
    docId = luigi.Parameter()
    countryId = luigi.Parameter()
    posId = luigi.Parameter()
    docMap = luigi.Parameter()
    inputPath = luigi.Parameter()
    backupInPath = luigi.Parameter()
    backupOutPath = luigi.Parameter()
    outputPath = luigi.Parameter()
    fileReader = FileReader()
    dataMapper = MapItem()

    def requires(self):
        # Back-up posted data
        return BackUpJSON(
            inputPath=self.inputPath,
            outputPath=self.backupInPath
        )

    def run(self):
        try:
            json_data_map = self.fileReader.readJSON(self.docMap)

            with self.input().open('r') as jsonFile:
                json_content = json.load(jsonFile)

            output_data = self.dataMapper.map(json_content, json_data_map)
            # print("\n\n\n---------------\n")
            # print(output_data)
            # print("\n---------------\n\n\n")
            # output_data = "crap...";

            with self.output().open('w') as f:
                f.write(output_data)

        except Exception as e:
            # TODO: use an error handler
            print(e)

        finally:
            # TODO: update document status
            print('TransformItem ready...')

    def output(self):
        return luigi.LocalTarget(path=str(self.backupOutPath))
