import os
import luigi

from time import sleep
from app.storages.localstorage import FileReader
from app.mappers.aptos.items import MapItem
from app.storages.sftpstorage import SFTPStorage
from tasks.aptos_transform import TransformItem


class ItemsTrigger(luigi.Task):
    docId = luigi.Parameter()
    countryId = luigi.Parameter()
    posId = luigi.Parameter()
    docMap = luigi.Parameter()
    inputPath = luigi.Parameter()
    backupInPath = luigi.Parameter()
    backupOutPath = luigi.Parameter()
    outputPath = luigi.Parameter()
    loadPath = luigi.Parameter()
    fileReader = FileReader()
    dataMapper = MapItem()
    ftpStorage = SFTPStorage()

    def requires(self):
        return TransformItem(
                docId=self.docId,
                countryId=self.countryId,
                posId=self.posId,
                docMap=self.docMap,
                inputPath=self.inputPath,
                backupInPath=self.backupInPath,
                backupOutPath=self.backupOutPath,
                outputPath=self.outputPath,
            )

    def run(self):
        try:
            self.ftpStorage.connect()
            self.ftpStorage.upload(str(self.backupOutPath), str(self.loadPath))
            os.remove(str(self.inputPath))

        except Exception as e:
            # TODO: use an error handler
            print(type(e).__name__)
            print(e)

        finally:
            print("Closing connection...")
            self.ftpStorage.close()

    def output(self):
        return luigi.LocalTarget(path=str(self.outputPath))

    # def complete(self):
    #     """Hack so we don't have to create input files manually.
    #
    #     Luigi will always think that this task is done, without checking for
    #     presence of source files.
    #     """
    #     return True
