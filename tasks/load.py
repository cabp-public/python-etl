import luigi

from time import sleep
from tasks.aptos_transform import TransformItem


class UploadViaSFTP(luigi.ExternalTask):
    docId = luigi.Parameter()
    countryId = luigi.Parameter()
    posId = luigi.Parameter()
    docMap = luigi.Parameter()
    inputPath = luigi.Parameter()
    backupInPath = luigi.Parameter()
    backupOutPath = luigi.Parameter()
    outputPath = luigi.Parameter()

    def requires(self):
        return TransformItem(
                docId=self.docId,
                countryId=self.countryId,
                posId=self.posId,
                docMap=self.docMap,
                inputPath=self.inputPath,
                backupInPath=self.backupInPath,
                backupOutPath=self.backupOutPath,
                outputPath=self.outputPath
            )

    def run(self):
        try:
            print('Uploading...')
            sleep(5)

        except Exception as e:
            # TODO: use an error handler
            print(e)

        finally:
            # TODO: update document status
            print('UploadFile ready...')

    def complete(self):
        return True
