
import json

from jsonschema import validate

itemPOSIdentityListSchema = open('ValidationSchemas/itemPOSIdentityList.json')
itemPOSIdentitySchema = open('ValidationSchemas/itemPOSIdentity.json')

def validateMetadata(metadata={}, metadataSchemaPath='ValidationSchemas/metadataSchema.json'):
    if metadata:
        metadataSchemaFile = open(metadataSchemaPath)
        metadataSchema = json.loads(metadataSchemaFile.read())
        validate(instance=metadata, schema=metadataSchema)
        
def validateItems(items=[], itemSchemaPath='ValidationSchemas/itemSchema.json'):
    if items:
        itemSchemaFile = open(itemSchemaPath)
        itemSchema = json.loads(itemSchemaFile.read())
        for item in items:
            validate(item, itemSchema)



def validateItemListsFiles(jsonFilesPath=[], itemListSchemaPath='ValidationSchemas/itemSchemaList.json'):
    itemListSchemaFile = open(itemListSchemaPath)
    itemListSchema = json.loads(str(itemListSchemaFile.read()))
    for jsonFilePath in jsonFilesPath:
        jsonFile = open(jsonFilePath)    
        try:
            print(jsonFilePath)
            itemList = json.loads(str(jsonFile.read()))
            validate(instance=itemList, schema=itemListSchema)
            validateMetadata(itemList.get('metadata', {}))
            validateItems(itemList.get('items', []))
        except Exception as e:
            print(e)
            raise(e)

if __name__ == "__main__":
    ## Probando los json corrrectos
    goodFiles = [
        '_SampleData/Archivo-de-Prueba-{}.json'.format(n) for n in range(1,5)
    ]

    validateItemListsFiles(goodFiles)


    # Testing Wrong Json Files
    wrongFiles = [
        '_SampleData/Archivo-de-Prueba-Clase-Malo-String-por-numerico2.json',
        '_SampleData/Archivo-de-Prueba-Sin-SKU-Malo-1.json'
    ]

    validateItemListsFiles(wrongFiles)