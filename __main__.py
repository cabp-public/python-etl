import uvicorn
from environment import *
from app.server.HTTP import serverHTTP

if __name__ == "__main__":
    uvicorn.run(serverHTTP)
