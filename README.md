# Python ETL Project
Custom ETL project using Luigi and Bocadillo

## Web Server
Scripts for the Bocadillo web server are now part of the main application. This makes easier to handle connections and models, but -most importantly, it's the best way we have now to manage package imports from both workflows and web server.
Web Server scripts can be found in this folder: `/app/server`

## Dynamic Workflows
Dynamic workflows follows a simple logic and can be scheduled via command line or dynamically. They require only 2 arguments to be scheduled: `--workflow` and `--document`. To get help about arguments simply use this command: `python __main__.py --h`

### Scheduling a workflow via web server
When starting the web server, the `--workflow` and `--document` arguments are ignored. You can also use the optional argument `--server`. The web server receives a POST request with a data payload, it validates the payload, extract customer from the database and inserts a new document record into it; then it uses the  `/app/core/workflow.py:WorkflowHandler` class to schedule the workflow. Go to the "root" folder and type-in the following command:

##### Format
Command syntax
```
python __main__.py
```

