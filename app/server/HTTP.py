from app.server.bootstrap import Bootstrap

routesDef = [
    {"pattern": "/", "methods": ["get"], "handler": "Home"},
    {
        "pattern": "/business/create/",
        "methods": ["post"],
        "handler": "customer_create.CustomerCreate"
    },
    {"pattern": "/business/update/", "methods": ["put"], "handler": "customer_create.CustomerCreate"},
    {"pattern": "/business/get/{id}/", "methods": ["get"], "handler": "customer_create.CustomerCreate"},
    {"pattern": "/business/delete/", "methods": ["delete"], "handler": "customer_create.CustomerCreate"},
    {
        "pattern": "/workflow/{parent_name}/{workflow_name}/{country_id}",
        "methods": ["post"],
        "handler": "aptos.DataIngestCountry"
    },
    {
        "pattern": "/workflow/{parent_name}/{workflow_name}/{country_id}/{pos_id}",
        "methods": ["post"],
        "handler": "aptos.DataIngestPos"
    }
]

serverBoot = Bootstrap(routesDef)
serverHTTP = serverBoot.go()
