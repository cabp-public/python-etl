import os
import json
from bocadillo.views import View


from app.db.models.business_node import BusinessNodeModel
from app.db.models.business_unit import BusinessUnitModel
from app.db.models.document import DocumentModel

from jsonschema import validate

DEFAULT_ERROR = 400


class CustomerCreate:
    async def post(self, req, res):
        output = { "success": False, "message": "" }
        status = DEFAULT_ERROR

        try:
            customerData = await req.json()

            # TODO: validate cusomer data
            if BusinessNodeModel.validate(customerData['customer']):

                customer = BusinessNodeModel(customerData['customer'])
                
                customerID = customer.save()
                
                if customerID is not False:
                    status = 200
                    output["success"] = True
                    output["message"] = "Customer created...!"

                    if 'POS' in customerData['customer']:
                        for POS in customerData['customer']['POS']:

                            POS['node_id'] = customerID
                            try:
                                newPOS = BusinessUnitModel(POS)
                                POSCreated = newPOS.save()
                            except Exception as e:
                                print('OnPosFailCreation'+ str(e))

                else:
                    output["message"] = "Customer was not created, see log for details"
            else:
                output["message"] = "Customer data is not valid..."
               

        except Exception as e:
            # TODO: use an error handler
            print(e)
            #output = json.loads(e)

        finally:
            res.json = output
            res.status_code = status

    async def put(self, req, res):
        output = { "success": False, "message": "" }
        status = DEFAULT_ERROR

        try:
            customerData = await req.json()

            # TODO: validate cusomer data
            if BusinessNodeModel.validate(customerData['customer']):

                    status = 200
                    output["success"] = True
                    output["message"] = "Customer Updated...!"
                   
            else:
                output["message"] = "Customer data is not valid..."
               

        except Exception as e:
            # TODO: use an error handler
            print(e)
            #output = json.loads(e)

        finally:
            res.json = output
            res.status_code = status

    async def get(self, req, res):
        output = { "success": False, "message": "" }
        status = DEFAULT_ERROR

        try:
            customerData = DocumentModel.get(req.id)

            # TODO: validate cusomer data
            print(customerData)    

        except Exception as e:
            # TODO: use an error handler
            print(e)
            #output = json.loads(e)

        finally:
            res.json = output
            res.status_code = status

    async def delete(self, req, res):
        output = { "success": False, "message": "" }
        status = DEFAULT_ERROR

        try:
            customerData = await req.json()

            # TODO: validate cusomer data
            if BusinessNodeModel.validate(customerData['customer']):

                    status = 200
                    output["success"] = True
                    output["message"] = "Customer Deleted...!"
                   
            else:
                output["message"] = "Customer data is not valid..."
               

        except Exception as e:
            # TODO: use an error handler
            print(e)
            #output = json.loads(e)

        finally:
            res.json = output
            res.status_code = status
