from bocadillo.views import View

DEFAULT_ERROR = 400

class NotFound:
    async def handle(self, req, res):
        output = { "success": False, "message": "404" }
        status = DEFAULT_ERROR

        try:
            status = 404
            output["message"] = "404 Not found =("

        except Exception as e:
            # TODO: use an error handler
            output = json.loads(e)

        finally:
            res.json = output
            res.status_code = status
