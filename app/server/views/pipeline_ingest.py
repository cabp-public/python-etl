import os
import json

from bocadillo.views import View

from app.core.workflow import WorkflowHandler
from app.storages.localstorage import FileWriter
from app.db.models.business_node import BusinessNodeModel
from app.db.models.document import DocumentModel

STORAGE_INPUT = os.getenv("STORAGE_INPUT")
DEFAULT_ERROR = 400

class PipelineIngest:
    async def post(self, req, res, posId: int):
        output = { "success": False, "message": "" }
        status = DEFAULT_ERROR
        workflowHandler = WorkflowHandler()
        fileWriter = FileWriter()

        try:
            payload = await req.json()
            # TODO: validate payload
            payloadValid = DocumentModel.validate(payload)

            if payloadValid and BusinessNodeModel.exists(posId):
                pos = BusinessNodeModel.get(posId)
                document = DocumentModel(payload)
                docId = document.save()
                fileWriter.write(STORAGE_INPUT + str(docId) + ".json", json.dumps(payload))

                if workflowHandler.exists(pos["workflow"]):
                    scheduleOutput = workflowHandler.schedule(
                        pos["workflow"],
                        int(docId),
                    )

                    output["success"] = True
                    output["message"] = "Workflow scheduled!"
                    output["detail"] = {
                        "posId": posId,
                        "docId": docId,
                        "workflow": pos["workflow"],
                        "scheduleOutput": scheduleOutput
                    }

                    status = 200
                else:
                    output["message"] = "Workflow doesn't exist"
            else:
                output["message"] = "POS doesn't exist"

        except Exception as e:
            # TODO: use an error handler
            #output = json.loads(e)
            print(e)
            status = 500

        finally:
            res.json = output
            res.status_code = status

    # async def handle(self, req, res):
    #     output = { "success": False, "message": "Default handler..." }
    #     status = DEFAULT_ERROR
    #
    #     try:
    #         status = 200
    #         res.json = output
    #         res.status_code = status
    #
    #     except Exception as e:
    #         # TODO: use an error handler
    #         res.json = json.loads(e)
    #         res.status_code = DEFAULT_ERROR
