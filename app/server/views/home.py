from bocadillo.views import View

class Home:
    async def get(self, req, res):
        output = { "message": "Welcome from GET...!" }

        try:
            res.json = output
            res.status_code = 200

        except Exception as e:
            # TODO: use an error handler
            res.json = json.loads(e)
            res.status_code = 500

    async def post(self, req, res):
        output = { "message": "Method not allowed" }

        try:
            res.json = output
            res.status_code = 400

        except Exception as e:
            # TODO: use an error handler
            res.json = json.loads(e)
            res.status_code = 500
