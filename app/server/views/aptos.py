import os
import json

from app.core.workflow import WorkflowHandler
from app.storages.localstorage import FileWriter
from app.db.models.business_unit import BusinessUnitModel
from app.db.models.document import DocumentModel

DEFAULT_STATUS = 400
DEFAULT_NOT_ALLOWED = {"message": "Method not allowed"}
DEFAULT_OUTPUT = {"success": False, "message": "", "details": {}}
STORAGE_INPUT = os.getenv("STORAGE_INPUT")


class DataIngest:
    @staticmethod
    def ingest(
            payload: json,
            parent_name: str,
            workflow_name: str,
            country_id: int,
            pos_id: int
    ):
        output = DEFAULT_OUTPUT
        workflow_handler = WorkflowHandler()
        file_writer = FileWriter()

        try:
            document = DocumentModel(payload)
            doc_id = document.save()
            input_path = STORAGE_INPUT + parent_name + "/" + str(doc_id) + ".json"
            file_writer.write(input_path, json.dumps(payload))

            if workflow_handler.exists(parent_name, workflow_name.lower()):
                schedule_output = workflow_handler.schedule(
                    parent_name.lower(),
                    workflow_name.lower(),
                    int(country_id),
                    int(pos_id),
                    int(doc_id),
                )

                output["success"] = True
                output["message"] = "Workflow scheduled!"
                output["details"] = schedule_output

            else:
                # TODO: update document status
                output["message"] = "Workflow doesn't exist"

        except Exception as e:
            # TODO: use an error handler
            print(type(e).__name__)
            print(e)

        finally:
            return output

    @staticmethod
    def respond_not_allowed(res):
        try:
            res.json = DEFAULT_NOT_ALLOWED
            res.status_code = DEFAULT_STATUS

        except Exception as e:
            # TODO: use an error handler
            print(e)

        finally:
            return res


class DataIngestCountry:
    async def post(self,
                   req,
                   res,
                   parent_name: str,
                   workflow_name: str,
                   country_id: int
                   ):
        output = DEFAULT_OUTPUT
        status = DEFAULT_STATUS

        try:
            payload = await req.json()
            payload_valid = True

            if payload_valid:
                ingest_results = DataIngest.ingest(
                    payload,
                    parent_name,
                    workflow_name,
                    country_id,
                    0
                )

                if ingest_results["success"]:
                    status = 200
                    output["success"] = True
                    output["message"] = "Workflow scheduled!"
                    output["details"] = ingest_results["details"]
                else:
                    # TODO: update document status
                    output["message"] = "Workflow NOT scheduled"
            else:
                # TODO: update document status
                output["message"] = "Payload not valid"

        except Exception as e:
            # TODO: use an error handler
            print(e)
            res.json = str(e)
            res.status_code = 500

        finally:
            res.json = output
            res.status_code = status

    async def get(self, req, res):
        res = DataIngest.respond_not_allowed(res)

    async def put(self, req, res):
        res = DataIngest.respond_not_allowed(res)

    async def delete(self, req, res):
        res = DataIngest.respond_not_allowed(res)


class DataIngestPos:
    async def post(self,
                   req,
                   res,
                   parent_name: str,
                   workflow_name: str,
                   country_id: int,
                   pos_id: int
                   ):
        output = DEFAULT_OUTPUT
        status = DEFAULT_STATUS

        try:
            payload = await req.json()
            payload_valid = True

            if payload_valid and BusinessUnitModel.exists(str(pos_id)):
                ingest_results = DataIngest.ingest(
                    payload,
                    parent_name,
                    workflow_name,
                    country_id,
                    pos_id
                )

                if ingest_results["success"]:
                    status = 200
                    output["success"] = True
                    output["message"] = "Workflow scheduled!"
                    output["details"] = ingest_results["details"]
                else:
                    # TODO: update document status
                    output["message"] = "Workflow NOT scheduled"
            else:
                output["message"] = "Data not valid or POS doesn't exist"

        except Exception as e:
            # TODO: use an error handler
            print(e)
            res.json = str(e)
            res.status_code = 500

        finally:
            res.json = output
            res.status_code = status

    async def get(self, req, res):
        res = DataIngest.respond_not_allowed(res)

    async def put(self, req, res):
        res = DataIngest.respond_not_allowed(res)

    async def delete(self, req, res):
        res = DataIngest.respond_not_allowed(res)
