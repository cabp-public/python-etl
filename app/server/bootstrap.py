import sys
from os import listdir
from os.path import isfile, join
from bocadillo import App
from bocadillo.routing import HTTPRoute
from bocadillo.views import View
from app.server.views import *

HAND_DEFAULT_KEY = "handler"
HAND_NOT_FOUND = "NotFound"
VIEWS_FOLDER = "./app/server/views/"


class Bootstrap:
    def __init__(self, routes):
        self.routesDef = routes
        self.viewsAvail = []
        self.routesAvail = []
        self.webServer = App()

        try:
            mod_main = __import__("app")
            mod_server = getattr(mod_main, "server")
            self.modViews = getattr(mod_server, "views")

        except Exception as e:
            # TODO: use an error handler
            print(e)

    def add_route(self, pattern: str, methods, handler):
        output = False

        try:
            if not isinstance(handler, View):
                view = View(handler, methods)
            else:
                view = handler

            route = HTTPRoute(pattern, view)
            self.webServer.router.add_route(route)
            output = True

        except Exception as e:
            # TODO: use an error handler
            output = e

        finally:
            return output

    def set_views(self):
        output = False

        try:
            allfiles = [f for f in listdir(VIEWS_FOLDER) if isfile(join(VIEWS_FOLDER, f))]
            allfiles.remove("__init__.py")

            for routeDef in self.routesDef:
                hand_name = routeDef[HAND_DEFAULT_KEY] if HAND_DEFAULT_KEY in routeDef else HAND_NOT_FOUND
                hand_name_parts = hand_name.split(".")

                # Module (file) not specified
                if len(hand_name_parts) == 1:
                    module_name = hand_name.lower()
                    class_name = hand_name
                else:
                    module_name = hand_name_parts[0]
                    class_name = hand_name_parts[1]

                view_module = getattr(self.modViews, module_name)
                view_class = getattr(view_module, class_name)

                self.add_route(
                    pattern=routeDef["pattern"],
                    methods=routeDef["methods"],
                    handler=view_class
                )

            output = True

        except Exception as e:
            # TODO: use an error handler
            print(e)
            output = e

        finally:
            return output

    def go(self):
        try:
            self.set_views()

        except Exception as e:
            # TODO: use an error handler
            print(e)

        finally:
            return self.webServer
