import sys
import argparse

from core.workflow import Workflow

argumentsParser = argparse.ArgumentParser()
argumentsParser.add_argument('--workflow',
    type=str,
    help='The name of the workflow you want to execute/schedule'
)
argumentsParser.add_argument('--schedule',
    type=int,
    help='Schedule the requested workflow'
)
arguments = argumentsParser.parse_args()

workflowHandler = Workflow()

print('hello from main...')

if workflowHandler.exists(arguments.workflow):
    workflowHandler.schedule(arguments.workflow)
else:
    print('workflow not avilable...')

# print('attempting to run ' + args['workflow'])
# aptosWrapperTask = AptosWrapperTask();
