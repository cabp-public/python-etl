from abc import ABC, abstractmethod

class Storage(ABC):
    credentials = ""
    backend = None
    root = ""
    name = ""
    policies = None

    @abstractmethod
    def put(self, path=""):
        pass

    @abstractmethod    
    def get(self, path=""):
        pass

    @abstractmethod    
    def push(self, path=""):
        pass
    
    @abstractmethod    
    def ls(self, path=""):
        pass
    
    @abstractmethod    
    def delete(self, path=""):
        pass