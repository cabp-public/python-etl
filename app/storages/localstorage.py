import json

from app.storages.storage import Storage
# from app.common.logger import Logger
class FileWriter():
    def write(self, outputPath, fileContents):
        done = False

        try:
            outputFile = open(outputPath, 'w+');
            outputFile.write(fileContents)
            outputFile.close();
            done = True

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return done

    def writeLn(self, outputPath, lineContents):
        done = False

        try:
            # Implement the actual write line stuff here...
            done = True

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return done

class FileReader():
    # Initializer / Instance Attributes
    def __init__(self):
        self.fileContents = ''

    def readJSON(self, inputPath):
        try:
            with open(inputPath) as jsonFile:
                self.fileContents = json.load(jsonFile)

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return self.fileContents

class LocalStorage(Storage):
    def __init__(self):
        super().__init__()

    def put(self, path):
        pass

    def get(path, asJSON = False):
        fileContents = False

        try:
            with open(path) as file:
                fileContents = json.load(jsonFile)
                # fileContents = json.load(jsonFile) if asJson else file.read()

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return fileContents

    def push(self, path):
        pass

    def ls(self, path):
        pass

    def delete(self, path):
        pass
