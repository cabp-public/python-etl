import os
import stat
import inspect
import paramiko

from app.storages.storage import Storage
from app.common.logger import Logger

FTP_HOST = os.getenv("FTP_HOST")
FTP_PORT = os.getenv("FTP_PORT")
FTP_USERNAME = os.getenv("FTP_USERNAME")
FTP_PASSWORD = os.getenv("FTP_PASSWORD")


class SFTPStorage:
    def __init__(self):
        self.client = None
        self.connection = None

        # paramiko.util.log_to_file('/tmp/paramiko.log') #TODO: add log path to conf
        # super().__init__()
        # self.name = name
        # self.credentials
        # self.backend = self._init_backend()
        # self.root_path = root_path
        # self.log = log if log else Logger().get_logger().new() #TODO: create unique ID

    def connect(self):
        try:
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.WarningPolicy)
            self.client.connect(FTP_HOST, FTP_PORT, FTP_USERNAME, FTP_PASSWORD)
            self.connection = self.client.open_sftp()

        except Exception as e:
            # TODO: use an error handler
            # TODO: log exception
            print(type(e).__name__)
            print(e)

    def close(self):
        try:
            self.client.close()

        except Exception as e:
            # TODO: use an error handler
            # TODO: log exception
            print(type(e).__name__)
            print(e)

    def upload(self, path_local: str, path_remote: str):
        try:
            result = self.connection.put(path_local, path_remote)
            # self.log.info("Archivo cargado exitosamente al servidor remoto")
            return result
            # return path_local + "\n" + path_remote

        except FileNotFoundError as e:
            print(type(e).__name__)
            print(e)
            # self.log.error("Archivo no encontrado", localpath=localpath, exception=e)

        except IsADirectoryError as e:
            print(type(e).__name__)
            print(e)
            # self.log.error("Localpath no es un archivo es un directorio", localpath=localpath, exception=e)

        except Exception as e:
            print(type(e).__name__)
            print(e)
            # self.log.error("Error desconocdio", exception=e)

    # def _init_backend(self):
    #     host = "sftp.grupo-ari.net"
    #     port = 1012
    #     password = "ASAP.2019ftp$"
    #     username = "ftpasap"
    #
    #     client = paramiko.SSHClient()
    #     client.set_missing_host_key_policy(paramiko.WarningPolicy)
    #
    #     client.connect(host, port=port, username=username, password=password)
    #
    #     return client.open_sftp()

    # def put(self, localpath, remotepath):
    #     try:
    #         _result= self.backend.put(localpath=localpath, remotepath=remotepath)
    #         self.log.info("Archivo cargado exitosamente al servidor remoto")
    #         return _result
    #     except FileNotFoundError as e:
    #         self.log.error(
    #             "Archivo no encontrado",
    #             localpath=localpath,
    #             exception=e)
    #     except IsADirectoryError as e:
    #         self.log.error(
    #             "Localpath no es un archivo es un directorio",
    #             localpath=localpath,
    #             exception=e)
    #     except Exception as e:
    #         self.log.error(
    #             "Error desconocdio",
    #             exception=e)


   
    # def get(self, path="", attempts=0):
    #     _source = '{}.{}'.format(
    #         type(self).__name__,
    #         inspect.stack()[0][3]
    #     ).lower()
    #     self.log.info(
    #         "Obteniendo archivo: {path}".format(path=path),
    #         source=_source
    #     )
    #     try:
    #         _result = self._operation(self.backend.file, self.get, path if path != "" else ".", attempts=attempts)
    #         self.log.info("Archivo recuperado exitósamente", source=_source)
    #         _result.prefetch()
    #         return _result
    #     except TimeoutError as e:
    #         self.log.exception(
    #             "Archivo no recuperado debido a Timeout con el servidor",
    #             source=_source,
    #             path=path,
    #             attempts=attempts,
    #             exception=e,
    #         )
    #         raise e #TODO: usar exepciones propias
    #     except Exception as e:
    #         self.log.exception(
    #             "Archivo no recuperado debido error desconocido",
    #             source=_source,
    #             path=path,
    #             attempts=attempts,
    #             exception=e,
    #         )
    #         raise e #TODO: usar exepciones propias
    #
    # def push(self, path=""):
    #     pass
    #
    # def _operation(self, sftp_method, fallback_method, path, attempts=0):
    #     _source = '{}.{}'.format(
    #         type(self).__name__,
    #         inspect.stack()[0][3]
    #     ).lower()
    #     try:
    #         _result = sftp_method(path)
    #         return _result
    #     except FileNotFoundError as e:
    #         self.log.error(
    #             "No se encuetra el archivo.",
    #             excepton=e,
    #             source=_source,
    #             fallback_method=fallback_method,
    #             sftp_method=sftp_method,
    #             path=path,
    #             attempts=attempts
    #         )
    #         raise e #TODO: create proper exception
    #     except Exception as e: #TODO: use proper SSH error
    #         self.log.error(
    #             "Error desconocido",
    #             excepton=e,
    #             source=_source,
    #             fallback_method=fallback_method,
    #             sftp_method=sftp_method,
    #             path=path,
    #             attempts=attempts
    #         )
    #         if (attempts < 3): #TODO: retrials configuration limit
    #             self.log.error("Reintentando...",  source=_source)
    #             self.backend = self._init_backend()
    #             fallback_method(path, attempts=attempts+1)
    #         else:
    #             raise e
    #
    #
    # def ls(self, path="", attempts=0):
    #     result = {"folders":{}, "files":{}}
    #     self.log.info(
    #         "Listando archivos en {path}".format(path=path),
    #         source='sftp.ls'
    #     )
    #     try:
    #         _result = self._operation(self.backend.listdir_attr, self.ls,path if path != "" else ".", attempts=attempts)
    #         for entry in _result:
    #             if stat.S_ISDIR(entry.st_mode):
    #                 result["folders"][entry.filename] = entry
    #             else:
    #                 result["files"][entry.filename] = entry
    #         self.log.info(
    #             "Directorio exitósamente listado",
    #             source='sftp.ls',
    #             content=result
    #         )
    #         return result
    #     except TimeoutError as e:
    #         self.log.exception(
    #             "Error listando directorio, por favor reintentar luego.",
    #             source='sftp.ls',
    #             exception=e
    #         )
    #     finally:
    #         return result
    #
    #
    # def delete(self, path):
    #     try:
    #         _result=self.backend.remove(path=path)
    #         self.log.info("Archivo borrado exitosamente")
    #         return _result
    #     except FileNotFoundError as e:
    #         self.log.error(
    #             "El path del archivo no existe",
    #             path=path,
    #             exception=e)
