import os
import json

KEY_METADATA = os.getenv("KEY_METADATA", 'metadata')
DATA_PREFIX = {
    "item": "Item",
    "itemposidentity": "ItemPOSIdentity",
    "departmentgroup": "DeptGroup",
    "department": "StoreDept",
    "class": "StoreClass",
    "currency": "StoreCurrency"
}


class MapItem:
    def __init__(self):
        self.outputData = ''

    def append_metadata(self, json_data):
        self.outputData = ''

        try:
            for (keyword, value) in json_data.items():
                self.outputData += '[' + str(keyword) + '] \n'

                for (key_data, value_data) in value.items():
                    self.outputData += str(key_data) + '=' + str(value_data) + '\n'

                self.outputData += '\n'

        except Exception as e:
            # TODO: use an error handler
            print(e)

    def map(self, json_content, json_data_map):
        try:
            content_metadata = json_content[KEY_METADATA]
            key_prefix = content_metadata['Task']['Module'].lower()
            items_avail = json_content[key_prefix]

            self.append_metadata(content_metadata)
            self.outputData += "[Data.1]\n"
            # self.outputData += key_prefix

            # Items
            for item in items_avail:
                # print(DATA_PREFIX[key_prefix])
                # print("\n")
                self.outputData += DATA_PREFIX[key_prefix]

                for (position, itemMap) in json_data_map.items():
                    item_name = itemMap['name']

                    if item_name in item:
                        value = item[item_name]
                    else:
                        value = ''

                    self.outputData += ',' + str(value)

                self.outputData += '\n'

            # print("\n---------------\n\n\n")
            return self.outputData

        except json.JSONDecodeError as e:
            # TODO: use an error handler
            print(type(e).__name__)
            print(e)

        return self.outputData
