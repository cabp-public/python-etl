import json

class MapJSONToDCN():
    # Initializer / Instance Attributes
    def __init__(self):
        self.outputData = ''

    def appendMetadata(self, jsonData):
        self.outputData = ''

        try:
            for (keyword, value) in jsonData.items():
                self.outputData += '[' + str(keyword) + '] \n'

                for (keydata, valuedata) in value.items():
                    self.outputData += str(keydata)  + '=' + str(valuedata) + '\n'

                self.outputData += '\n'

        except Exception as e:
            # TODO: use an error handler
            print(e)

    def map(self, jsonContent, jsonSchema):
        try:
            contentMetadata = jsonContent['metadata']
            itemsAvail = jsonContent['items']

            self.appendMetadata(contentMetadata)

            # Items
            for item in itemsAvail:
                self.outputData += 'Item'

                # print(jsonSchema.items())
                for (position, itemMap) in jsonSchema.items():
                    itemName = itemMap['name']

                    if itemName in item:
                        value = item[itemName]
                    else:
                        value = ''

                    self.outputData += ',' + str(value)

                self.outputData += '\n'

            # self.outputData = 'content'

            return self.outputData

        except json.JSONDecodeError as e:
            # TODO: use an error handler
            print(e)

        return self.outputData
