import sys

from app.core.workflows import *

WORKFLOWS_PATH = "app.core.workflows."
WORKFLOW_CLASS_SUFFIX = "Workflow"


class WorkflowHandler:
    def exists(self, parent_name: str, workflow_name: str):
        output = False

        try:
            mod_name = WORKFLOWS_PATH + parent_name

            if mod_name in sys.modules:
                # class_name = workflow_name[:1].upper() + workflow_name[1:]
                class_name = "Generic"
                class_name += WORKFLOW_CLASS_SUFFIX
                mod_workflow = sys.modules[mod_name]
                output = hasattr(mod_workflow, class_name)

        except AttributeError as e:
            # TODO: use an error handler
            # TODO: log exception
            print(type(e).__name__)
            print(e)

        finally:
            return output

    def schedule(self,
                 parent_name: str,
                 workflow_name: str,
                 country_id: int,
                 pos_id: int,
                 doc_id: int
                 ):
        output = False

        try:
            mod_name = WORKFLOWS_PATH + parent_name
            # class_name = workflow_name[:1].upper() + workflow_name[1:]
            class_name = "Generic"
            class_name += WORKFLOW_CLASS_SUFFIX
            mod_workflow = sys.modules[mod_name]
            workflow_class = getattr(mod_workflow, class_name)
            workflow = workflow_class()
            output = workflow.schedule(parent_name, workflow_name, country_id, pos_id, doc_id)
            # output = workflow.schedule(parent_name, country_id, pos_id, doc_id)

        except Exception as e:
            # TODO: use an error handler
            print(type(e).__name__)
            print(e)

        return output
