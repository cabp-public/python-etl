import os
import luigi

from tasks.aptos import ItemsTrigger

LOCAL_SCHEDULER = bool(os.getenv("LOCAL_SCHEDULER"))
DEFAULT_OUTPUT = {"success": False, "message": "", "details": {}, "data": []}
STORAGE_INPUT = os.getenv("STORAGE_INPUT")
STORAGE_SOURCE = os.getenv("STORAGE_SOURCE")
STORAGE_BACKUP = os.getenv("STORAGE_BACKUP")
STORAGE_OUTPUT = os.getenv("STORAGE_OUTPUT")
STORAGE_INVALID = os.getenv("STORAGE_INVALID")
STORAGE_DATA_MAPS = os.getenv("STORAGE_DATA_MAPS")
KEY_SUCCESS = os.getenv("KEY_SUCCESS", "success")
KEY_DATA = os.getenv("KEY_DATA", "data")
KEY_DOCUMENT = os.getenv("KEY_DOCUMENT", "document")
KEY_LOCATIONS = os.getenv("KEY_LOCATIONS", "locations")
WORKFLOWS_PATH = "app.core.workflows."
WORKFLOW_CLASS_SUFFIX = "Workflow"
FTP_ROOT_PREFIX = os.getenv("FTP_ROOT_PREFIX", "data/aptos/output/")


class GenericWorkflow:
    documentId = {}
    setUpData = {}
    workflowTasks = []

    def set_up(self,
               parent_name: str,
               workflow_name: str,
               country_id: int,
               pos_id: int,
               doc_id: int
               ):
        output = DEFAULT_OUTPUT
        storage_path = parent_name + "/"
        backup_input_path = STORAGE_BACKUP + storage_path + "input/"
        backup_output_path = STORAGE_BACKUP + storage_path + "output/"
        load_path = "/" + FTP_ROOT_PREFIX + str(country_id)
        map_path = STORAGE_DATA_MAPS + storage_path + workflow_name + ".json"

        try:
            if pos_id > 0:
                load_path += "/" + str(pos_id) + "/"

            # TODO: extract data from DB, using doc_id
            self.setUpData = {
                "document": {
                    "id": str(doc_id),
                    "country_id": str(country_id),
                    "pos_id": str(pos_id),
                    "map": map_path,
                    "locations": {
                        "input": STORAGE_INPUT + storage_path + str(doc_id) + ".json",
                        "inputBackup": backup_input_path + str(doc_id) + ".json",
                        "outputBackup": backup_output_path + str(doc_id) + ".dcn",
                        "source": STORAGE_SOURCE + storage_path + str(doc_id) + ".json",
                        "output": STORAGE_OUTPUT + storage_path + str(doc_id) + ".dcn",
                        "invalid": STORAGE_INVALID + storage_path + str(doc_id) + ".json",
                        "load": load_path + str(doc_id) + ".dcn"
                    }
                }
            }

            output[KEY_SUCCESS] = True

        except Exception as e:
            # TODO: use an error handler
            # TODO: log exception
            print(type(e).__name__)
            print(e)

        finally:
            return output

    def schedule(self,
                 parent_name: str,
                 workflow_name: str,
                 country_id: int,
                 pos_id: int,
                 doc_id: int
                 ):
        output = DEFAULT_OUTPUT

        try:
            class_name = workflow_name[:1].upper() + workflow_name[1:]
            class_name += WORKFLOW_CLASS_SUFFIX
            print("\nAptosItemsWorkflow: scheduling myself...")

            set_up_ready = self.set_up(parent_name, workflow_name, country_id, pos_id, doc_id)

            if set_up_ready[KEY_SUCCESS]:
                tasks = [ItemsTrigger(
                    docId=self.setUpData[KEY_DOCUMENT]["id"],
                    countryId=self.setUpData[KEY_DOCUMENT]["country_id"],
                    posId=self.setUpData[KEY_DOCUMENT]["pos_id"],
                    docMap=self.setUpData[KEY_DOCUMENT]["map"],
                    inputPath=self.setUpData[KEY_DOCUMENT][KEY_LOCATIONS]["input"],
                    backupInPath=self.setUpData[KEY_DOCUMENT][KEY_LOCATIONS]["inputBackup"],
                    backupOutPath=self.setUpData[KEY_DOCUMENT][KEY_LOCATIONS]["outputBackup"],
                    outputPath=self.setUpData[KEY_DOCUMENT][KEY_LOCATIONS]["output"],
                    loadPath=self.setUpData[KEY_DOCUMENT][KEY_LOCATIONS]["load"]
                )]

                luigi.build(tasks, local_scheduler=LOCAL_SCHEDULER)

                output = {
                    "scheduled": True,
                    "details": {
                        "startData": self.setUpData,
                        "tasksToExecute": len(tasks)
                    }
                }
            else:
                output["details"] = set_up_ready["details"]

        except Exception as e:
            # TODO: use an error handler
            # TODO: log exception
            print(type(e).__name__)
            print(e)

        return output
