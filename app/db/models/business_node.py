import os

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from app.db.engine import Base
from app.db.engine import db_session

from app.db.models.business_unit import BusinessUnitModel


class BusinessNodeModel(Base):
    __tablename__ = 'business_node'
    node_id = Column(Integer, primary_key=True)
    node_name = Column(String(100), nullable=True)
    node_input = Column(Integer, nullable=False)
    node_input_format = Column(Integer, nullable=True)
    node_output = Column(Integer, nullable=True)
    node_output_format = Column(Integer, nullable=True)
    node_status = Column(Integer, nullable=True)
    node_functional_id = Column(String(100), nullable=True)

    busines_units = relationship('BusinessUnitModel', backref=backref('business_unit'))


    def __init__(self, businessNodeData = {}):
        try:
            self.node_name = businessNodeData['name']
            self.node_input = 1
            self.node_input_format = None
            self.node_output = None
            self.node_output_format = None
            self.node_status = 0
            self.node_functional_id = businessNodeData['id']
            

        except Exception as e:
            # TODO: use an error handler
            print(e)
            
    @staticmethod
    def exists(id):
        output = False

        try:
            # TODO: get client data from DB
            output = True

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    @staticmethod
    def get(id):
        output = False
        
        try:
            # TODO: get client data from DB
            output = {}
            print('Retreiving customer...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    @staticmethod
    def exists(id):
        output = False
        print('on exist')
        try:
            # TODO: get client data from DB
            output = BusinessNodeModel.query.filter_by(node_functional_id=id).first()
            
            return output
        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output


    def save(self):
        output = False
        try:
            exists = BusinessNodeModel.exists(self.node_functional_id)
            if exists is None:
                    db_session.add(self)
                    db_session.commit()
                    output = self.node_id
                    print('Saving client data...'+str(output))
            else:
                print('Customer Exists')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def delete(self):
        output = False

        try:
            # TODO: perform soft delete
            output = True
            print('Deleting customer...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def all(self):
        output = False

        try:
            # TODO: get all customers from DB
            output = []
            print('Retreiving ALL customers...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def list(self):
        output = False

        try:
            # TODO: get a list of clients: id, name
            output = []
            print('Retreiving customer list...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output
