import os
import datetime
import json

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from jsonschema import validate

from app.db.engine import Base
from app.db.engine import db_session

STORAGE_JSON_SCHEMAS = os.getenv("STORAGE_JSON_SCHEMAS")


class DocumentModel(Base):
    __tablename__ = 'documents'
    doc_id = Column(Integer, primary_key=True)
    doc_type = Column(Integer, nullable=True)
    doc_created_by = Column(Integer, nullable=True)
    doc_parent = Column(Integer, nullable=True)
    doc_original = Column(Integer, nullable=True)
    doc_expiration = Column(String(100), nullable=True)
    doc_cloned = Column(Integer, nullable=True)
    doc_processed = Column(Integer, nullable=True)
    doc_format = Column(Integer, nullable=True)
    doc_status = Column(Integer, nullable=True)

    unit_id = Column(Integer, ForeignKey(
        'business_unit.unit_id'), nullable=False)

    business_units = relationship(
        'BusinessUnitModel', foreign_keys=unit_id)

    def __init__(self, data = {}):
        try:
            # TODO: define data for remaining fields
            self.doc_type = None
            self.doc_created_by = None
            self.doc_parent = None
            self.doc_original = None
            self.doc_expiration = None
            self.doc_cloned = None
            self.doc_processed = None
            self.doc_format = 1
            self.doc_status = 0
            self.unit_id = 1

        except Exception as e:
            # TODO: use an error handler
            print(type(e).__name__)
            print(e)

    @staticmethod
    def exists(self, id):
        output = False

        try:
            # TODO: find if POS exists
            output = True

        except Exception as e:
            # TODO: use an error handler
            print(e)
            print(type(e).__name__)
            print(e)

        return output

    @staticmethod
    def get(self, id):
        output = False
           
        try:
            # TODO: get client data from DB
            output =  output = DocumentModel.query.filter_by(doc_id=id).first()
       
        except Exception as e:
            # TODO: use an error handler
           output = e

        finally:
            return output

    def save(self):
        output = False

        try:
            db_session.add(self)
            db_session.commit()
            output = self.doc_id

        except Exception as e:
            # TODO: use an error handler
            print(type(e).__name__)
            print(e)

        finally:
            return output

    def delete(self):
        output = False

        try:
            # TODO: perform soft delete
            output = True
            print('Deleting customer...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def all(self):
        output = False

        try:
            # TODO: get all customers from DB
            output = []
            print('Retreiving ALL customers...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def list(self):
        output = False

        try:
            # TODO: get a list of clients: id, name
            output = []
            print('Retreiving customer list...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output
