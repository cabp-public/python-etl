import os

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from app.db.engine import Base
from app.db.engine import db_session


class BusinessUnitModel(Base):
    __tablename__ = 'business_unit'
    unit_id = Column(Integer, primary_key=True, autoincrement=True)
    unit_name = Column(String(64), nullable=True)
    unit_status = Column(Integer, nullable=True)
    unit_functional_id = Column(String(100), nullable=True)
    # TODO: put properties here
    node_id = Column(Integer, ForeignKey(
        'business_node.node_id'), nullable=False)
    business_node = relationship(
        "BusinessNodeModel", foreign_keys=node_id)
    
    def __init__(self, data = {}):
        try:
            #TODO: initialize an empty customer
            self.unit_name = data['name']
            self.node_id = data['node_id']
            self.unit_status = 0
            self.unit_functional_id= data['id']
            
            
            print("I'm a new POS!")

        except Exception as e:
            # TODO: use an error handler
            print(e)

    @staticmethod
    def exists(functional_id: str):
        output = False

        try:
            # TODO: use select count and return true|false
            # row = BusinessUnitModel.query.filter_by(unit_functional_id=functional_id).first()
            output = True

            return output

        except Exception as e:
            # TODO: use an error handler
            print(e)

        finally:
            return output

    @staticmethod
    def get(id):
        output = False

        try:
            # TODO: get client data from DB
            output = {
                "id": 260,
                "name": "Sports",
                "location": {
                    "country": "USA",
                    "state": "NY",
                    "city": "NY",
                    "address": "888 Main St."
                },
                "loadMethod": False,
                "workflow": 1
            }

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def save(self):
        output = False

        try:
            exists = BusinessUnitModel.exists(self.unit_functional_id)
            if exists is None:
                    db_session.add(self)
                    db_session.commit()
                    output = self.unit_name
                    print('Saving Unit data...'+str(output))
            else:
                print('Business Unit already exists')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def delete(self):
        output = False

        try:
            # TODO: perform soft delete
            output = True
            print('Deleting customer...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def all(self):
        output = False

        try:
            # TODO: get all customers from DB
            output = []
            print('Retreiving ALL customers...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output

    def list(self):
        output = False

        try:
            # TODO: get a list of clients: id, name
            output = []
            print('Retreiving customer list...')

        except Exception as e:
            # TODO: use an error handler
            print(e)

        return output
