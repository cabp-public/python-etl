import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import urllib

DB_SERVER = os.getenv("DB_SERVER")
DB_DATABASE = os.getenv("DB_DATABASE")
DB_UID = os.getenv("DB_UID")
DB_PWD = os.getenv("DB_PWD")

# This is necessary to celare DB Driver as for DB connection
params = urllib.parse.quote_plus(
    "DRIVER={SQL Server};SERVER=" + DB_SERVER + ";DATABASE=" + DB_DATABASE + ";UID=" + DB_UID + ";PWD=" + DB_PWD)

engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" %
                       params, pool_pre_ping=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata. Otherwise
    # you will have to import them first before calling init_db()
    import models
    Base.metadata.create_all(bind=engine)
